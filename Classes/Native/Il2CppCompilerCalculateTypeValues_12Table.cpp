﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2241034664.h"
#include "UnityEngine_UnityEngine_SerializeField3073427462.h"
#include "UnityEngine_UnityEngine_Hash1282836532937.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "UnityEngine_UnityEngine_WWWTranscoder1124214756.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_Application354826772.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767.h"
#include "UnityEngine_UnityEngine_DebugLogHandler865810509.h"
#include "UnityEngine_UnityEngine_Debug1368543263.h"
#include "UnityEngine_UnityEngine_Display3666191348.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDel3423469815.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Input1785128008.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator1251553160.h"
#include "UnityEngine_UnityEngine_Random1170710517.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView1159903376.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas2267846590.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas2542964478.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerFai1733255396.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd1806615588.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inter1835954262.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inters365858839.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier259698391.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit4134400622.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification317971878.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification2254252895.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire2947922465.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Gener788733994.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play2968292729.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri4067966717.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033.h"
#include "UnityEngine_UnityEngine_AudioType4076847944.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1693994278.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest254341728.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest2654069489.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest3177762630.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandler3552561393.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandlerRa3420491431.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1216180266.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler3443159558.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2365358851.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1224150142.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1520641178.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2585285711.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe2122816804.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent2656950.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3043633143.h"
#include "UnityEngine_UnityEngine_SetupCoroutine3582942563.h"
#include "UnityEngine_UnityEngine_WritableAttribute3715198420.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1557026495.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3198293052.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_960725851.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local3019851150.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP3365630962.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie1333316625.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie3110978151.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score2307748940.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade4160680639.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3505065032.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState455716270.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (ResourceRequest_t2560315377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1200[2] = 
{
	ResourceRequest_t2560315377::get_offset_of_m_Path_1(),
	ResourceRequest_t2560315377::get_offset_of_m_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (Resources_t339470017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (SerializePrivateVariables_t2241034664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (SerializeField_t3073427462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (Hash128_t2836532937)+ sizeof (Il2CppObject), sizeof(Hash128_t2836532937_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1205[4] = 
{
	Hash128_t2836532937::get_offset_of_m_u32_0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t2836532937::get_offset_of_m_u32_1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t2836532937::get_offset_of_m_u32_2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t2836532937::get_offset_of_m_u32_3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (WWW_t2919945039), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (WWWForm_t3950226929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1207[6] = 
{
	WWWForm_t3950226929::get_offset_of_formData_0(),
	WWWForm_t3950226929::get_offset_of_fieldNames_1(),
	WWWForm_t3950226929::get_offset_of_fileNames_2(),
	WWWForm_t3950226929::get_offset_of_types_3(),
	WWWForm_t3950226929::get_offset_of_boundary_4(),
	WWWForm_t3950226929::get_offset_of_containsFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (WWWTranscoder_t1124214756), -1, sizeof(WWWTranscoder_t1124214756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1208[8] = 
{
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_ucHexChars_0(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_lcHexChars_1(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlEscapeChar_2(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlSpace_3(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlForbidden_4(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpEscapeChar_5(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpSpace_6(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpForbidden_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (UnityString_t276356480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (AsyncOperation_t3814632279), sizeof(AsyncOperation_t3814632279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1210[1] = 
{
	AsyncOperation_t3814632279::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (Application_t354826772), -1, sizeof(Application_t354826772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1211[2] = 
{
	Application_t354826772_StaticFields::get_offset_of_s_LogCallbackHandler_0(),
	Application_t354826772_StaticFields::get_offset_of_s_LogCallbackHandlerThreaded_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (LogCallback_t1867914413), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (Behaviour_t955675639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (Camera_t189460977), -1, sizeof(Camera_t189460977_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1214[3] = 
{
	Camera_t189460977_StaticFields::get_offset_of_onPreCull_2(),
	Camera_t189460977_StaticFields::get_offset_of_onPreRender_3(),
	Camera_t189460977_StaticFields::get_offset_of_onPostRender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (CameraCallback_t834278767), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (DebugLogHandler_t865810509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (Debug_t1368543263), -1, sizeof(Debug_t1368543263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1217[1] = 
{
	Debug_t1368543263_StaticFields::get_offset_of_s_Logger_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (Display_t3666191348), -1, sizeof(Display_t3666191348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1218[4] = 
{
	Display_t3666191348::get_offset_of_nativeDisplay_0(),
	Display_t3666191348_StaticFields::get_offset_of_displays_1(),
	Display_t3666191348_StaticFields::get_offset_of__mainDisplay_2(),
	Display_t3666191348_StaticFields::get_offset_of_onDisplaysUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (DisplaysUpdatedDelegate_t3423469815), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (MonoBehaviour_t1158329972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (Input_t1785128008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (HideFlags_t1434274199)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1222[10] = 
{
	HideFlags_t1434274199::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (Object_t1021602117), sizeof(Object_t1021602117_marshaled_pinvoke), sizeof(Object_t1021602117_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1223[2] = 
{
	Object_t1021602117::get_offset_of_m_CachedPtr_0(),
	Object_t1021602117_StaticFields::get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (Component_t3819376471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (GameObject_t1756533147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (Transform_t3275118058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (Enumerator_t1251553160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1227[2] = 
{
	Enumerator_t1251553160::get_offset_of_outer_0(),
	Enumerator_t1251553160::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (Random_t1170710517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (YieldInstruction_t3462875981), sizeof(YieldInstruction_t3462875981_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (ADBannerView_t1159903376), -1, sizeof(ADBannerView_t1159903376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1230[4] = 
{
	ADBannerView_t1159903376::get_offset_of__bannerView_0(),
	ADBannerView_t1159903376_StaticFields::get_offset_of_onBannerWasClicked_1(),
	ADBannerView_t1159903376_StaticFields::get_offset_of_onBannerWasLoaded_2(),
	ADBannerView_t1159903376_StaticFields::get_offset_of_onBannerFailedToLoad_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (BannerWasClickedDelegate_t2267846590), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (BannerWasLoadedDelegate_t2542964478), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (BannerFailedToLoadDelegate_t1733255396), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (ADInterstitialAd_t1806615588), -1, sizeof(ADInterstitialAd_t1806615588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1234[3] = 
{
	ADInterstitialAd_t1806615588::get_offset_of_interstitialView_0(),
	ADInterstitialAd_t1806615588_StaticFields::get_offset_of_onInterstitialWasLoaded_1(),
	ADInterstitialAd_t1806615588_StaticFields::get_offset_of_onInterstitialWasViewed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (InterstitialWasLoadedDelegate_t1835954262), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (InterstitialWasViewedDelegate_t365858839), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (CalendarIdentifier_t259698391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1237[12] = 
{
	CalendarIdentifier_t259698391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (CalendarUnit_t4134400622)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1238[12] = 
{
	CalendarUnit_t4134400622::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (LocalNotification_t317971878), -1, sizeof(LocalNotification_t317971878_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1239[2] = 
{
	LocalNotification_t317971878::get_offset_of_notificationWrapper_0(),
	LocalNotification_t317971878_StaticFields::get_offset_of_m_NSReferenceDateTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (RemoteNotification_t2254252895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1240[1] = 
{
	RemoteNotification_t2254252895::get_offset_of_notificationWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (DirectorPlayer_t2947922465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (Playable_t3667545548)+ sizeof (Il2CppObject), sizeof(Playable_t3667545548_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1242[2] = 
{
	Playable_t3667545548::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Playable_t3667545548::get_offset_of_m_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (GenericMixerPlayable_t788733994)+ sizeof (Il2CppObject), sizeof(GenericMixerPlayable_t788733994_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1243[1] = 
{
	GenericMixerPlayable_t788733994::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (Playables_t2968292729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (ScriptPlayable_t4067966717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (LoadSceneMode_t2981886439)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1246[3] = 
{
	LoadSceneMode_t2981886439::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (SceneManager_t90660965), -1, sizeof(SceneManager_t90660965_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1247[3] = 
{
	SceneManager_t90660965_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t90660965_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t90660965_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (Scene_t1684909666)+ sizeof (Il2CppObject), sizeof(Scene_t1684909666_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1248[1] = 
{
	Scene_t1684909666::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (QueryTriggerInteraction_t478029726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1249[4] = 
{
	QueryTriggerInteraction_t478029726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (AudioSettings_t3144015719), -1, sizeof(AudioSettings_t3144015719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1250[1] = 
{
	AudioSettings_t3144015719_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (AudioConfigurationChangeHandler_t3743753033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (AudioType_t4076847944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1252[14] = 
{
	AudioType_t4076847944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (AudioClip_t1932558630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1253[2] = 
{
	AudioClip_t1932558630::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t1932558630::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (PCMReaderCallback_t3007145346), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (PCMSetPositionCallback_t421863554), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (AnimatorStateInfo_t2577870592)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t2577870592_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1256[9] = 
{
	AnimatorStateInfo_t2577870592::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (Animator_t69676727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (AnimatorControllerPlayable_t4078305555)+ sizeof (Il2CppObject), sizeof(AnimatorControllerPlayable_t4078305555_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1258[1] = 
{
	AnimatorControllerPlayable_t4078305555::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (AnimationPlayable_t1693994278)+ sizeof (Il2CppObject), sizeof(AnimationPlayable_t1693994278_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1259[1] = 
{
	AnimationPlayable_t1693994278::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (UnityWebRequest_t254341728), sizeof(UnityWebRequest_t254341728_marshaled_pinvoke), sizeof(UnityWebRequest_t254341728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1260[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	UnityWebRequest_t254341728::get_offset_of_m_Ptr_6(),
	UnityWebRequest_t254341728_StaticFields::get_offset_of_domainRegex_7(),
	UnityWebRequest_t254341728_StaticFields::get_offset_of_forbiddenHeaderKeys_8(),
	UnityWebRequest_t254341728::get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9(),
	UnityWebRequest_t254341728::get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10(),
	UnityWebRequest_t254341728_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (UnityWebRequestMethod_t2654069489)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1261[6] = 
{
	UnityWebRequestMethod_t2654069489::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (UnityWebRequestError_t3177762630)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1262[30] = 
{
	UnityWebRequestError_t3177762630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { sizeof (UploadHandler_t3552561393), sizeof(UploadHandler_t3552561393_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1264[1] = 
{
	UploadHandler_t3552561393::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (UploadHandlerRaw_t3420491431), sizeof(UploadHandlerRaw_t3420491431_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (DownloadHandler_t1216180266), sizeof(DownloadHandler_t1216180266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1266[1] = 
{
	DownloadHandler_t1216180266::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (DownloadHandlerBuffer_t3443159558), sizeof(DownloadHandlerBuffer_t3443159558_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (DownloadHandlerTexture_t2365358851), sizeof(DownloadHandlerTexture_t2365358851_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (DownloadHandlerAssetBundle_t1224150142), sizeof(DownloadHandlerAssetBundle_t1224150142_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (DownloadHandlerAudioClip_t1520641178), sizeof(DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (WrapperlessIcall_t2585285711), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (ThreadAndSerializationSafe_t2122816804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (AttributeHelperEngine_t958797062), -1, sizeof(AttributeHelperEngine_t958797062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1273[3] = 
{
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (DisallowMultipleComponent_t2656950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (RequireComponent_t864575032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1275[3] = 
{
	RequireComponent_t864575032::get_offset_of_m_Type0_0(),
	RequireComponent_t864575032::get_offset_of_m_Type1_1(),
	RequireComponent_t864575032::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (ExecuteInEditMode_t3043633143), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1277[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (SetupCoroutine_t3582942563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (WritableAttribute_t3715198420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (AssemblyIsEditorAssembly_t1557026495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (GcUserProfileData_t3198293052)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1281[4] = 
{
	GcUserProfileData_t3198293052::get_offset_of_userName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t3198293052::get_offset_of_userID_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t3198293052::get_offset_of_isFriend_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t3198293052::get_offset_of_image_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (GcAchievementDescriptionData_t960725851)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1282[7] = 
{
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Title_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Image_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_AchievedDescription_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_UnachievedDescription_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Hidden_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t960725851::get_offset_of_m_Points_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (GcAchievementData_t1754866149)+ sizeof (Il2CppObject), sizeof(GcAchievementData_t1754866149_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1283[5] = 
{
	GcAchievementData_t1754866149::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t1754866149::get_offset_of_m_PercentCompleted_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t1754866149::get_offset_of_m_Completed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t1754866149::get_offset_of_m_Hidden_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t1754866149::get_offset_of_m_LastReportedDate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (GcScoreData_t3676783238)+ sizeof (Il2CppObject), sizeof(GcScoreData_t3676783238_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1284[7] = 
{
	GcScoreData_t3676783238::get_offset_of_m_Category_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_ValueLow_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_ValueHigh_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_Date_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_FormattedValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_PlayerID_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t3676783238::get_offset_of_m_Rank_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (CameraClearFlags_t452084705)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1285[6] = 
{
	CameraClearFlags_t452084705::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (TextureFormat_t1386130234)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1286[47] = 
{
	TextureFormat_t1386130234::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (LocalUser_t3019851150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1287[3] = 
{
	LocalUser_t3019851150::get_offset_of_m_Friends_5(),
	LocalUser_t3019851150::get_offset_of_m_Authenticated_6(),
	LocalUser_t3019851150::get_offset_of_m_Underage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (UserProfile_t3365630962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1288[5] = 
{
	UserProfile_t3365630962::get_offset_of_m_UserName_0(),
	UserProfile_t3365630962::get_offset_of_m_ID_1(),
	UserProfile_t3365630962::get_offset_of_m_IsFriend_2(),
	UserProfile_t3365630962::get_offset_of_m_State_3(),
	UserProfile_t3365630962::get_offset_of_m_Image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (Achievement_t1333316625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1289[5] = 
{
	Achievement_t1333316625::get_offset_of_m_Completed_0(),
	Achievement_t1333316625::get_offset_of_m_Hidden_1(),
	Achievement_t1333316625::get_offset_of_m_LastReportedDate_2(),
	Achievement_t1333316625::get_offset_of_U3CidU3Ek__BackingField_3(),
	Achievement_t1333316625::get_offset_of_U3CpercentCompletedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (AchievementDescription_t3110978151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1290[7] = 
{
	AchievementDescription_t3110978151::get_offset_of_m_Title_0(),
	AchievementDescription_t3110978151::get_offset_of_m_Image_1(),
	AchievementDescription_t3110978151::get_offset_of_m_AchievedDescription_2(),
	AchievementDescription_t3110978151::get_offset_of_m_UnachievedDescription_3(),
	AchievementDescription_t3110978151::get_offset_of_m_Hidden_4(),
	AchievementDescription_t3110978151::get_offset_of_m_Points_5(),
	AchievementDescription_t3110978151::get_offset_of_U3CidU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (Score_t2307748940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1291[6] = 
{
	Score_t2307748940::get_offset_of_m_Date_0(),
	Score_t2307748940::get_offset_of_m_FormattedValue_1(),
	Score_t2307748940::get_offset_of_m_UserID_2(),
	Score_t2307748940::get_offset_of_m_Rank_3(),
	Score_t2307748940::get_offset_of_U3CleaderboardIDU3Ek__BackingField_4(),
	Score_t2307748940::get_offset_of_U3CvalueU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (Leaderboard_t4160680639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1292[10] = 
{
	Leaderboard_t4160680639::get_offset_of_m_Loading_0(),
	Leaderboard_t4160680639::get_offset_of_m_LocalUserScore_1(),
	Leaderboard_t4160680639::get_offset_of_m_MaxRange_2(),
	Leaderboard_t4160680639::get_offset_of_m_Scores_3(),
	Leaderboard_t4160680639::get_offset_of_m_Title_4(),
	Leaderboard_t4160680639::get_offset_of_m_UserIDs_5(),
	Leaderboard_t4160680639::get_offset_of_U3CidU3Ek__BackingField_6(),
	Leaderboard_t4160680639::get_offset_of_U3CuserScopeU3Ek__BackingField_7(),
	Leaderboard_t4160680639::get_offset_of_U3CrangeU3Ek__BackingField_8(),
	Leaderboard_t4160680639::get_offset_of_U3CtimeScopeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (SendMouseEvents_t3505065032), -1, sizeof(SendMouseEvents_t3505065032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1293[5] = 
{
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_s_MouseUsed_0(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_LastHit_1(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_MouseDownHit_2(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_CurrentHit_3(),
	SendMouseEvents_t3505065032_StaticFields::get_offset_of_m_Cameras_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (HitInfo_t1761367055)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1294[2] = 
{
	HitInfo_t1761367055::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HitInfo_t1761367055::get_offset_of_camera_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (UserState_t455716270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1297[6] = 
{
	UserState_t455716270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
